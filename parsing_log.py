import texttable as tt

def read_file(file_name):
    file = open(file_name,'r')
    ip_adress = {}
    pages = {}
    empty = 0
    while empty<3:
        data = file.readline().split()
        if data == []:
            empty +=1
        else:
            if data[0] in ip_adress:
                ip_adress[data[0]] += 1
            else:
                ip_adress[data[0]] = 1
            if '"GET' in data:
                index = data.index('"GET') + 1
                if data[index] in pages:
                    pages[data[index]] += 1
                else:
                    pages[data[index]] = 1
            elif '"OPTIONS' in data:
                index = data.index('"OPTIONS') + 1
                if data[index] in pages:
                    pages[data[index]] += 1
                else:
                    pages[data[index]] = 1
            elif '"-"' in data:
                if "" in pages:
                    pages['""'] += 1
                else:
                    pages['""'] = 1
            empty = 0
    file.close()
    return ip_adress, pages

ip, pages = read_file("log.txt")
ip = sorted(ip.items(), key=lambda item: item[1], reverse=True) #sort
table_ip = tt.Texttable()
table_ip.header(['IP adress','Frequency'])
for i in range(len(ip)):
    row = ip[i]
    table_ip.add_row([row[0], row[1]])
print(table_ip.draw())
pages = sorted(pages.items(), key=lambda item: item[1], reverse=True)
table_pages = tt.Texttable()
table_pages.header(['Adress','Frequency'])
for i in range(len(pages)):
    row = pages[i]
    table_pages.add_row([row[0], row[1]])
print(table_pages.draw())
